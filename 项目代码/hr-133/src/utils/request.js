import axios from 'axios'
import store from '@/store'
import router from '@/router'
// 创建一个axios实例对象
// 1. baseURL  整个应用api的基地址
// 2. 大数处理  (不是必须的)
// 为什么会产生大数问题？
//  1. js能处理的Number范围 有一个临界值
//  2. JSON.parse()
// 怎么解决的大数问题呢？
//  判断当前浏览器是否支持BigNumber类型  如果支持就使用这种新数据类型进行处理
//  如果不支持就处理成字符串类型 toString()

// 3. timeout  接口最大超时时间 从接口正式发起到数据返回 最大可以持续多久
// 如果baseURL传入的是一个相对路径 前端项目启动的地址 + 相对路径  = 'http://localhost:8081/api'
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL, // url = base url + request url  y
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// 请求拦截器
// token全局注入  判断一下有没有token 如果有
// 为了其它接口方便携带token进行数据请求  放到了request header中
service.interceptors.request.use(
  config => {
    // 在这里我们从本地取到token  然后放到request headers中
    // congfig: axios封装的对象 对象里面有一个比较重要的headers属性 请求头
    // 修改完config之后 必须要return才可以

    // 获取token数据 注入reques headers中
    // 1. 从vuex中获取数据token
    const token = store.getters.token
    console.log('title数据为:', store.getters.title)
    // 2. 存入headers
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// 响应拦截器
// 1. token失效判断 401状态 -> 跳回到登录页
// 2. token续签 (token refresh token)
// 3. 当后端接口不是以常规的200状态码标识接口成功时，需要我们做额外的处理
service.interceptors.response.use(
  // 以http状态码作为判断依据 200 - 300 (200 201)
  // response会作为具体的接口函数拿到的接口数据
  response => {
    console.log('拦截器里面拿到的初始数据', response)
    // res.data.data  res.data
    // 如果success字段为true 代表接口是成功的 可以直接把response.data返回
    // 如果success字段为false 代表接口是失败的  return Promise.reject()
    const success = response.data.success
    if (success) {
      return response.data
    } else {
      return Promise.reject(response.data.message)
    }
  },
  // 如果不在200-300之间 (401)
  error => {
    console.dir(error)
    if (error.response.status === 401) {
      // 1. 清理失效的数据
      store.commit('user/removeUserInfo')
      // 2. 跳回到登录
      router.push('/login')
    }
    return Promise.reject(error)
  }
)

export default service
