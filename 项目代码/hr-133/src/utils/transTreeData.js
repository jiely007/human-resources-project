
export function transTree(souceData) {
  const targetData = []
  // 树形结构生成

  // 核心思路：
  // 1. 先遍历原数组 以原数组中的每一项的id作为对象的key, 每一项本身作为对象的value形成一个对象结构(map)
  // 2. 遍历原数组 使用数组中的每一项的pid 去第一步形成的map结构去匹配key(id) 如果匹配上
  // 就把当前项放入找到节点的children属性中去  如果无法完成匹配 代表当前项就是最根上的父节点
  // 那就把当前项直接放到最终产出的targetData中去
  const map = {}
  souceData.forEach(item => {
    map[item.id] = item
    item.children = []
  })
  // 通过pid去匹配id
  souceData.forEach(item => {
    if (map[item.pid]) {
      // 匹配上
      map[item.pid].children.push(item)
    } else {
      // 没有匹配上
      targetData.push(item)
    }
  })
  // 返回的是处理之后的数组
  return targetData
}

