// 通过定义插件的形式完成组件的全局注册
import PageTools from './PageTools'
import UploadExcel from './UploadExcel'
import UploadImg from './UploadImg'
import FullScreen from './FullScreen'
import Lang from './Lang'

// 1. 对象定义

export const plugin1 = {
  install(Vue) {
    console.log('插件1被注册了', Vue)
    console.dir(Vue)
    // Vue.component -> 全局组件注册的
    // Vue.compoennt('page-tools',PageTools)
  }
}

// 2. 函数定义

export function plugin2(Vue) {
  console.log('插件2被注册了', Vue)
}

// 全局注册组件插件
const compoenntPlugin = {
  install(Vue) {
    // 插件
    // 使用Vue构造函数身上的方法或者直接操作原型进行一些vue功能的增强
    // Vue.compoenent
    // Vue.directive
    // Vue.proptotype.$http ->  .vue  this.$http
    Vue.component('page-tools', PageTools)
    Vue.component('upload-excel', UploadExcel)
    Vue.component('upload-img', UploadImg)
    Vue.component('full-screen', FullScreen)
    Vue.component('lang', Lang)
    // 在这里完成所有的需要进行全局注册的组件
  }
}

export default compoenntPlugin
