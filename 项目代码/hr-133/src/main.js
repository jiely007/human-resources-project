// 引入vue框架文件
import Vue from 'vue'

// 样式重置文件
import 'normalize.css/normalize.css' // A modern alternative to CSS resets

// 引入ElementUi三方组件库
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

// 引入应用的样式文件
import '@/styles/index.scss' // global css

// 根组件
import App from './App'
// vuex文件
import store from './store'
// 路由文件
import router from './router'

// 引入所有可用图标
import '@/icons' // icon

// 引入权限控制文件
// 引入模块时 这里表示直接执行permission js文件中的代码
import '@/permission' // permission control

// set ElementUI lang to EN
// 配置elementUI语言包
Vue.use(ElementUI)
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

// 测试环境变量的使用
console.log('当前环境下的port', process.env, process.env.VUE_APP_PORT)

// yarn start -> 开发环境  -> port -> 8080
// yarn build:prod  -> 生成环境 -> port -> 8081

// 引入插件
import componentPlugin from '@/components'
// 注册
Vue.use(componentPlugin)
// 引入全局指令
import directivePlugin from '@/directive'
Vue.use(directivePlugin)

// 引入语言模块
import i18n from '@/lang'
Vue.config.productionTip = false

// vue实例化
new Vue({
  el: '#app',
  router,
  store,
  i18n, // 这里的名字必须叫 i18n
  render: h => h(App)
})
