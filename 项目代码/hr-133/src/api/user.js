// 导入reques模块
// request已经具备了 baseURL + timeout + 请求拦截器 + 响应拦截器
import request from '@/utils/request'

// 登录函数
/**
 * @description: 登录函数
 * @param {*} data { mobile,password}
 * @return {*} promise
 */
export function login(data) {
  // 这个位置的传参  axios({请求必要的参数})
  // request 函数执行之后 返回的是一个promise对象
  // 发送请求之后返回的promise对象 又作为login函数的返回值 ？
  return request({
    url: '/sys/login',
    method: 'POST',
    data
  })
}

// import {login} from 'user.js'

// login(reqData).then(res=>{})

/**
 * @description: 获取用户资料
 * @param {*} token
 * @return {*}
 */
export function getUserInfo() {
  return request({
    url: '/sys/profile',
    method: 'POST'
  })
}

/**
 * @description: 获取用户头像
 * @param {*} id 用户id
 * @return {*}
 */
export function getUserDetailById(id) {
  return request({
    url: `/sys/user/${id}`
  })
}

/**
 * @description: 保存员工信息
 * @param {*} data
 * @return {*}
 */
export function saveUserDetailById(data) {
  return request({
    url: `/sys/user/${data.id}`,
    method: 'put',
    data
  })
}

