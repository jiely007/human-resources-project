import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import employees from './modules/employees'
import departments from './modules/department'
import settings from './modules/settings'
import permissions from './modules/permissions'
import salarys from './modules/salarys'

// 动态路由 (根据不同用户的权限进行变化的 数量并不是固定的 需要进行筛选)
export const asyncRoutes = [
  departments,
  settings,
  employees,
  permissions,
  salarys
]

// 静态路由 (每个用户都可以正常访问 不会发生变化)
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
  {
    path: '/uploadExcel',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '',
        name: 'uploadExcel',
        component: () => import('@/views/uploadExcel'),
        meta: { title: '导入', icon: 'dashboard' }
      }
    ]
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: [...constantRoutes] // 动 + 静
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
// 重置路由的方法
// 退出登录时把之前的路由记录干掉

// A -> ['工资','社保']  B -> ['工资']  -> ['工资','社保']

export function resetRouter() {
  // 得到一个全新的router实例对象
  const newRouter = createRouter()
  // 使用新的路由记录覆盖掉老的路由记录
  router.matcher = newRouter.matcher // reset router
}

export default router
