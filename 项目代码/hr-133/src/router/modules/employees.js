import Layout from '@/layout'

const employees = {
  path: '/employees',
  component: Layout,
  children: [{
    path: '', // 如果children path置空的话 当前这个路由会作为一级渲染的默认路由
    name: 'employees',
    component: () => import('@/views/employees'),
    meta: { title: '员工管理', icon: 'people' }
  }, {
    path: 'employeeDetail',
    name: 'employeeDetail',
    component: () => import('@/views/employees/detail.vue'),
    hidden: true
  }]
}

export default employees
