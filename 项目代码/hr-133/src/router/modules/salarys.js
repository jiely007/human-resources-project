import Layout from '@/layout'
const salarys = {
  path: '/salarys',
  component: Layout,
  children: [{
    path: '', // 如果children path置空的话 当前这个路由会作为一级渲染的默认路由
    name: 'salarys',
    component: () => import('@/views/salarys'),
    meta: { title: '工资管理', icon: 'skill' }
  }]
}

export default salarys
