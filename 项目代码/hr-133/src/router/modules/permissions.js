import Layout from '@/layout'
const permissions = {
  path: '/permissions',
  component: Layout,
  children: [{
    path: '', // 如果children path置空的话 当前这个路由会作为一级渲染的默认路由
    name: 'permissions',
    component: () => import('@/views/permissions'),
    meta: { title: '权限点管理', icon: 'people' }
  }]
}

export default permissions
