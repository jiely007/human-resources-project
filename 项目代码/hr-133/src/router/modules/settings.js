import Layout from '@/layout'

const settings = {
  path: '/settings',
  component: Layout,
  children: [{
    path: '', // 如果children path置空的话 当前这个路由会作为一级渲染的默认路由
    name: 'settings',
    component: () => import('@/views/settings'),
    meta: { title: '角色管理', icon: 'setting' }
  }]
}

export default settings
