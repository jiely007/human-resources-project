import Layout from '@/layout'
const departments = {
  path: '/departments',
  component: Layout,
  children: [{
    path: '', // 如果children path置空的话 当前这个路由会作为一级渲染的默认路由
    name: 'departments',
    component: () => import('@/views/departments'),
    meta: { title: '组织架构', icon: 'tree' }
  }]
}

export default departments
