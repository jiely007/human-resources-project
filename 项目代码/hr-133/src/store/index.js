import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import menu from './modules/menu'

Vue.use(Vuex)

const store = new Vuex.Store({
  // 组合模块的配置项
  modules: {
    app,
    settings,
    user,
    menu
  },
  // vuex中state的计算属性 state中的数据变化 getters自动跟着变化
  // 创建对于模块中数据的快捷访问
  // eg: user state name  $store.state.user.name -> $store.getters.name
  getters
})

export default store
