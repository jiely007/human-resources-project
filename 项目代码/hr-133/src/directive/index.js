// 定义全局指令
import store from '@/store'
const directivePlugin = {
  install(Vue) {
    Vue.directive('check-btn', {
      inserted(el, binding) {
        console.log(el, binding.value)
        // 编写按钮显示隐藏逻辑
        // 思路: 根据当前按钮自身的权限标识去points去查找 如果能找到代表有权限显示
        // 如果找不到就需要隐藏
        const points = store.state.user.userInfo.roles.points
        console.log(points)
        if (!points.includes(binding.value)) {
          // 没有找到就隐藏
          el.parentNode.removeChild(el)
        }
      }
    })
  }
}
export default directivePlugin

