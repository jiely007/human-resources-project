// 使用koa 搭建一个静态服务器
const Koa = require('koa')
const app = new Koa()
const serve = require('koa-static')
// 引入进行代理配置的中间件
const proxy = require('koa2-proxy-middleware')
// 使用proxy中间件 把所有接口地址中 包含api路径地址 转发到 真实的接口地址上

const { historyApiFallback } = require('koa2-connect-history-api-fallback')
// 这句话 的意思是除接口之外所有的请求都发送给了 index.html
app.use(historyApiFallback({ 
  // 白名单  只有路径是/api的path才会被识别为后端路由请求 否则一律转发回index.html文件
  whiteList: ['/api']
}))

app.use(proxy({
  targets: {
    '/api/(.*)': {
        target: 'http://ihrm-java.itheima.net', //后端服务器地址
        changeOrigin: true
    }
  }
}))

// 把根目录下的public文件夹静态化  他里面放置的所有的静态文件可以在线访问
// 做为静态文件访问的默认路径  其中index.html文件将被作为默认打开页面
app.use(serve(__dirname + "/public")) //将public下的代码静态化


app.listen(3000)