# 1. MongoDB软件安装

> 我们的人资本地服务依赖的数据库不是我们之前学习的mysql，是一个新的数据库叫做MongoDB，所以咱们第一步就是先把这个数据库软件安装一下

下面是网盘地址，里面有我们需要的软件，下载到本地

链接：https://pan.baidu.com/s/1nrW9FsogOVxQy-onGFOETA 
提取码：2y2z 

<img src="assets/01.png" style="zoom:58%;" />

<img src="assets/02.png" style="zoom:58%;" />

<img src="assets/03.png" style="zoom:58%;" />

<img src="assets/04.png" style="zoom:58%;" />

<img src="assets/05.png" style="zoom:58%;" />

<img src="assets/06.png" style="zoom:58%;" />

# 2. 配置环境变量

> 安装完mongoDB之后需要配置mongoDB的环境变量，具体操作就是找到安装mongoDB的目录下的bin文件夹，将该目录配置到 环境变量中  并目录地址为 `C:\Program Files\MongoDB\Server\4.2\bin`

​                             <img src="assets/07.png" style="zoom:58%;" />

​          <img src="assets/08.png" style="zoom:58%;" />

​                      <img src="assets/09.png" style="zoom:58%;" />

<img src="assets/11.png" style="zoom:58%;" />

<img src="assets/12.png" style="zoom:58%;" />

# 3. Nodejs本地服务

> 前俩步我们已经安装好的必要的数据库软件并且配置了全局变量，接下来我们就可以启动服务端代码了，打开`server`目录，我们支持俩个命令， npm run start  和 npm run serve 
>
> npm run start : 重置所有数据恢复默认并启动服务器接口
>
> npm run serve : 单纯的启动服务，不重置数据库数据

 启动成功截图

<img src="assets/13.png" style="zoom:58%;" />

 大家在开发的时候，只需要在代理的位置换成我们的地址即可

<img src="assets/code.png" style="zoom:58%;" /> 

 